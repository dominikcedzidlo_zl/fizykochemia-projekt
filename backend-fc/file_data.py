import csv
import numpy as np


with open('../in.txt', 'r') as f:
    reader = csv.reader(f, delimiter=' ')
    f_tc = []  # Wartości temperatur *C
    f_c = []  # Wartości ciepła właściwego
    for f_tc_n, f_c_n in reader:
        f_c.append(float(f_c_n))
        f_tc.append(float(f_tc_n))


f_tc = np.array(f_tc)
f_c = np.array(f_c)
f_t = np.array(f_tc) + 273.15
