import hug
import sympy as sp
import numpy as np
import scipy.interpolate
from typing import Callable, Iterable
from sympy.parsing.latex import parse_latex
from file_data import f_t, f_tc, f_c

from hug.middleware import CORSMiddleware

api = hug.API(__name__)
api.http.add_middleware(CORSMiddleware(api))


@hug.get('/hello')
def hello(text: hug.types.text):
    """Returns text back"""
    return {
        "text": text,
    }


@hug.get('/file_data')
def get_file_data():
    """Returns data from file"""
    return {
        "f_t": f_t,
        "f_tc": f_tc,
        "f_c": f_c,
    }


def hotfix_latex(l):
    return l.replace(r'\left(', '(').replace(r'\right)', ')')


hotfix_subs = [
    (parse_latex(hotfix_latex(sp.latex(latex_function_to_hotfix))), latex_function_to_hotfix)
    for latex_function_to_hotfix in [
        sp.Min,
        sp.Max,
    ]
]


def hotfix_parse_latex(l):
    pl = parse_latex(hotfix_latex(l))
    for s in hotfix_subs:
        pl = pl.subs(*s)
    return pl


@hug.post('/podstaw')
def latex(main: hug.types.text, subs):
    """Returns computed latex"""
    exp = hotfix_parse_latex(main)
    exp_subbed = exp

    exps_subs = [hotfix_parse_latex(sub).args for sub in subs]
    exp_subbed = exp_subbed.subs(exps_subs)

    return {
        "input_latex": sp.latex(exp),
        "computed_latex": sp.latex(exp_subbed),
    }


def calculate_H(c: Callable, h: Callable, T: Iterable):
    # c(t) - Ciepło właściwe w danej temperaturze
    # h(t) - Całkowita dodana entalpia użytkownika w danej temperaturze
    T = iter(T)
    t_0 = next(T)
    H = [c(t_0)*t_0]  # Entalpia
    t_p = t_0  # Poprzednia wartość t
    for t_n in T:
        H.append(
            H[-1] +  # Poprzednia entalpia
            h(t_n) - h(t_p) +  # Wartość dodanej entalpi użytkownika w danej temperaturze
            c((t_n + t_p)/2) * (t_n - t_p)  # Całka C_p * Delta T
        )
        t_p = t_n
    return H


exp_Td = parse_latex(r'T_d')
exp_T1 = parse_latex(r'T_1')
exp_T2 = parse_latex(r'T_2')
exp_Hr = parse_latex(r'H_r')


@hug.post('/oblicz')
def oblicz(main: hug.types.text, subs):
    """Returns computed latex"""
    exp = hotfix_parse_latex(main)
    exp_subbed = exp

    exps_subs = [hotfix_parse_latex(sub).args for sub in subs]
    exp_subbed = exp_subbed.subs(exps_subs)
    exp_integrated = sp.integrate(exp_subbed, exp_Td)

    i_t0 = float(exp_T1.subs(exps_subs))
    i_t1 = float(exp_T2.subs(exps_subs))
    i_td = i_t1 - i_t0
    i_h = float(exp_Hr.subs(exps_subs))

    c = scipy.interpolate.interp1d(f_t, f_c)
    t_min = f_t[0]
    t_max = f_t[-1]
    T = np.arange(t_min, t_max + 1)

    # Zerowa
    def h0(T):
        return 0

    # Punktowo na początku reakcji
    def h1(T):
        if T < i_t0:
            return 0
        else:
            return i_h

    # Stała
    def h2(T):
        if T < i_t0:
            return 0
        elif T > i_t1:
            return i_h
        else:
            return (T - i_t0) * i_h / i_td

    # User
    def h3(T):
        if T < i_t0:
            return 0
        elif T > i_t1:
            return float(exp_integrated.subs(exp_Td, i_t1 - i_t0))
        else:

            return float(exp_integrated.subs(exp_Td, T - i_t0))

    try:
        mnoznik = i_h/h3(i_t1)
        def h4(T):
            return h3(T) * mnoznik
    except:
        h4 = h0

    H0 = np.array(calculate_H(c, h0, T))
    H1 = np.array(calculate_H(c, h1, T))
    H2 = np.array(calculate_H(c, h2, T))
    H3 = np.array(calculate_H(c, h3, T))
    H4 = np.array(calculate_H(c, h4, T))

    return {
        "input_latex": sp.latex(exp),
        "computed_latex": sp.latex(exp_subbed),
        "integral_latex": sp.latex(exp_integrated),
        "hs": [
            {"H": H0, "T": T, "name": "Zerowa"},
            {"H": H1, "T": T, "name": "Na początku reakcji"},
            {"H": H2, "T": T, "name": "Stała"},
            {"H": H3, "T": T, "name": "Użytkownika"},
            {"H": H4, "T": T, "name": "Użytkownika z auto mnożnikiem"},
        ],
        "dzielnik": h3(i_t1)/i_h,
        "mnoznik": mnoznik,
    }


exp_x = parse_latex(r'x')

ksztaltTypes = {
    "punktowy": {"name": "Punktowy", "settings_type": "numer01"},
    "latex": {"name": "Wzór", "settings_type": "latex"},
}


@hug.get('/ksztalty')
def ksztalty():
    return {
        "selects": [
            {
                "name": "Na początku",
                "type": "punktowy",
                "setting": 0,
                "settings_type": ksztaltTypes["punktowy"]["settings_type"],
            },
            {
                "name": "Liniowy",
                "type": "latex",
                "setting": "1",
                "settings_type": ksztaltTypes["latex"]["settings_type"],
                "integrate": True,
            },
            {
                "name": "Przykład 1",
                "type": "latex",
                "setting": "1-x",
                "settings_type": ksztaltTypes["latex"]["settings_type"],
                "integrate": True,
            },
            {
                "name": "Przykład 2",
                "type": "latex",
                "setting": "(1-x)^2",
                "settings_type": ksztaltTypes["latex"]["settings_type"],
                "integrate": True,
            },
        ],
    }


def generate_h_from_zmiana(zmiana):
    setting = zmiana["ksztalt"]["setting"]
    i_t0 = zmiana['temp'][0] + 273.15
    i_t1 = zmiana['temp'][1] + 273.15
    i_td = i_t1 - i_t0
    i_h = zmiana['ent']

    def get_x(T):
        return (T - i_t0) / i_td

    type_ = zmiana["ksztalt"]["type"]
    if type_ == "punktowy":
        def f(x):
            if x < setting:
                return 0
            else:
                return 1
    elif type_ == "latex":
        exp = hotfix_parse_latex(setting)
        if zmiana["ksztalt"].get("integrate", True):
            exp_integrated = sp.integrate(exp, exp_x)
        else:
            exp_integrated = exp
        max_value = float(exp_integrated.subs(exp_x, 1))
        exp_integrated_normalized = exp_integrated / max_value
        print(max_value)

        zmiana["ksztalt"]["out_exp"] = sp.latex(exp_integrated)

        def f(x):
            return float(exp_integrated_normalized.subs(exp_x, x))

    def h(T):
        x = get_x(T)
        if x < 0:
            return 0
        elif x > 1:
            return f(1) * i_td
        else:
            return f(x) * i_td

    return h


@hug.post('/oblicz2')
def oblicz2(zmiany):

    print(zmiany)

    hs = [generate_h_from_zmiana(zmiana) for zmiana in zmiany if zmiana["ksztalt"]]

    print(zmiany)

    print(hs)

    def h_total(T):
        return sum(h(T) for h in hs)

    for a in hs:
        print(a(1800))

    c = scipy.interpolate.interp1d(f_t, f_c)
    t_min = f_t[0]
    t_max = f_t[-1]
    T = np.arange(t_min, t_max + 1)

    H0 = np.array(calculate_H(c, lambda T: 0, T))
    H_total = np.array(calculate_H(c, h_total, T))

    t = T - 273.15

    H = H_total - H0
    dH = []
    for i in range(1,len(H)):
        dH.append(H[i]-H[i-1])

    r = {
        "hs": [
            {"H": H0, "T": t, "name": "Zerowa"},
            {"H": H_total, "T": t, "name": "Z przemianami"},
        ],
        "wykresy": [
            {
                "name": "Wykres 1",
                "data": [
                    {"x": t, "y": H0, "name": "Zerowa"},
                    {"x": t, "y": H_total, "name": "Z przemianami"},
                ],
            },
            {
                "name": "Wykres 2",
                "data": [
                    {"x": t, "y": H, "name": ""},
                ],
            },
            {
                "name": "Wykres 3",
                "data": [
                    {"x": t, "y": dH, "name": ""},
                ],
            }
        ],
        "zmiany": zmiany,
    }

    # print(r)

    return r
