// https://stackoverflow.com/a/17606289
function replaceAll (target, search, replacement) {
  return target.replace(new RegExp(search, 'g'), replacement)
}

export default function (a, b) {
  return replaceAll(a, ' ', '') === replaceAll(b, ' ', '')
}
