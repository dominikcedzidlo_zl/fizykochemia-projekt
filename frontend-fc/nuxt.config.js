const pkg = require('./package')

module.exports = {
  mode: 'spa',

  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:8000'
  },

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      // KaTeX
      {
        rel:"stylesheet",
        href: "https://cdn.jsdelivr.net/npm/katex@0.9.0/dist/katex.min.css",
        integrity: "sha384-TEMocfGvRuD1rIAacqrknm5BQZ7W7uWitoih+jMNFXQIbNl16bO8OZmylH/Vi/Ei",
        crossorigin: "anonymous",
      }
    ],
    script: [
      // Plotly.js
      {
        src: "https://cdn.plot.ly/plotly-1.41.3.min.js",
      },
      // KaTeX
      {
        src: "https://cdn.jsdelivr.net/npm/katex@0.9.0/dist/katex.min.js",
        integrity: "sha384-jmxIlussZWB7qCuB+PgKG1uLjjxbVVIayPJwi6cG6Zb4YKq0JIw+OMnkkEC7kYCq",
        crossorigin: "anonymous",
      },
      // MathQuill
      { src: "https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" },
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    'element-ui/lib/theme-chalk/index.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/element-ui'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // https://buefy.github.io/documentation/start
    'nuxt-buefy',
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
